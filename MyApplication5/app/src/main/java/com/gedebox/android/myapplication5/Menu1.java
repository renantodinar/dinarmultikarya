package com.gedebox.android.myapplication5;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by andhika on 3/11/2016.
 */
public class Menu1 extends Fragment {
    String nomer=new String();
    String operator=new String();
    String nominal=new String();

    private String STATE_NOMER = "noHP";
    private String STATE_NOMINAL = "nominal";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_menu_1, container, false);

        Spinner spinner = (Spinner) v.findViewById(R.id.dropdown1);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.dropdown1, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        Button button = (Button) v.findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Perhatian");
                final EditText text = (EditText)getActivity().findViewById(R.id.textfield1);
                Spinner spinner1= (Spinner)getActivity().findViewById(R.id.dropdown1);
                nomer= text.getText().toString();

                //cek apakah nomer kosong
                if(nomer.equals("")){
                    alertDialog.setMessage("silahkan memasukkan nomer hp tujuan");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }else
                //cek apakah nomer kurang dari 10 digit
                if(nomer.length()<10){
                    alertDialog.setMessage("jumlah angka no hp tujuan salah/kurang");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }

                else{


                    operator = prefixOperator(nomer);
                    nominal = spinner1.getSelectedItem().toString();
                    alertDialog.setMessage("Apakah no " + operator + " " + nomer + " nominal " + nominal + " adalah benar? saldo tidak dapat dikembalikan apabila nomer yang dimasukkan salah");
               /* alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Benar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });*/
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Benar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    SmsManager smsManager = SmsManager.getDefault();
                                    smsManager.sendTextMessage("082188881550", null, operator + "//" + nominal.replace(".", "") + "//" + nomer, null, null);
                                    text.setText("");
                                    Toast.makeText(getActivity(), "Permintaan Dikirim", Toast.LENGTH_LONG).show();

                                    Fragment newFragment = new LoadingFragment();
                                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                    ft.add(R.id.content_frame, newFragment).commit();

                                    dialog.dismiss();
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Salah",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    /*Fragment newFragment = new LoadingFragment();
                                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                    ft.add(R.id.content_frame, newFragment).commit();*/
                                    dialog.dismiss();
                                }
                            });
                }
                alertDialog.show();
            }
        });

        return v;
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        //return inflater.inflate(R.layout.fragment_menu_1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Pulsa");

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        //get text from edittext
        EditText text=(EditText)getActivity().findViewById(R.id.textfield1);
        nomer=text.getText().toString();
        // Save the user's current game state
        savedInstanceState.putString(STATE_NOMER, nomer);
        savedInstanceState.putString(STATE_NOMINAL, nominal);
        // Always call the superclass so it can save the view hierarchy state

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //restore value
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            nomer = savedInstanceState.getString(STATE_NOMER);
            nominal = savedInstanceState.getString(STATE_NOMINAL);
            EditText text=(EditText)getActivity().findViewById(R.id.textfield1);
            text.setText("not null cuy");
        } else {
            //EditText text=(EditText)getActivity().findViewById(R.id.textfield1);
            //text.setText("null cuy 2");
            // Probably initialize members with default values for a new instance
        }
    }

    public String prefixOperator(String x){
        String namaoperator="";
        String arrayOperator[][]= new String[50][50];
        arrayOperator[0][0]="TELKOMSEL";
        arrayOperator[0][1]="0811";
        arrayOperator[0][2]="0812";
        arrayOperator[0][3]="0813";
        arrayOperator[0][4]="0821";
        arrayOperator[0][5]="0822";
        arrayOperator[0][6]="0823";
        arrayOperator[0][7]="0851";
        arrayOperator[0][8]="0852";
        arrayOperator[0][9]="0853";

        arrayOperator[1][0]="INDOSAT";
        arrayOperator[1][1]="0855";
        arrayOperator[1][2]="0856";
        arrayOperator[1][3]="0857";
        arrayOperator[1][4]="0858";
        arrayOperator[1][5]="0814";
        arrayOperator[1][6]="0815";
        arrayOperator[1][7]="0816";

        arrayOperator[2][0]="XL";
        arrayOperator[2][1]="0817";
        arrayOperator[2][2]="0818";
        arrayOperator[2][3]="0819";
        arrayOperator[2][4]="0859";
        arrayOperator[2][5]="0877";
        arrayOperator[2][6]="0878";

        arrayOperator[3][0]="TRI";
        arrayOperator[3][1]="0896";
        arrayOperator[3][2]="0897";
        arrayOperator[3][3]="0898";
        arrayOperator[3][4]="0899";

        arrayOperator[4][0]="SMARTFREN";
        arrayOperator[4][1]="0881";
        arrayOperator[4][2]="0882";
        arrayOperator[4][3]="0883";
        arrayOperator[4][4]="0884";
        arrayOperator[4][5]="0885";
        arrayOperator[4][6]="0886";
        arrayOperator[4][7]="0887";
        arrayOperator[4][8]="0888";
        arrayOperator[4][9]="0889";

        arrayOperator[5][0]="AXIS";
        arrayOperator[5][1]="0838";
        arrayOperator[5][2]="0831";
        arrayOperator[5][3]="0832";

        arrayOperator[6][0]="BOLT";
        arrayOperator[6][1]="0998";
        arrayOperator[6][2]="0999";



        for(int i=0;i<arrayOperator.length;i++){
            for(int j=0;j<arrayOperator[i].length;j++){
                if(x.substring(0,4).equals(arrayOperator[i][j])){
                    namaoperator=arrayOperator[i][0];
                }
            }
        }

        return namaoperator;
    }
}
