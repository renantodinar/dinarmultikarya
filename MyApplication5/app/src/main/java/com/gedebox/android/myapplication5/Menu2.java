package com.gedebox.android.myapplication5;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by andhika on 3/11/2016.
 */
public class Menu2 extends Fragment {
    String pemilikrek=new String();
    String nominal=new String();
    String bank=new String();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        //return inflater.inflate(R.layout.fragment_menu_2, container, false);
        View v = inflater.inflate(R.layout.fragment_menu_2, container, false);

        Spinner spinner = (Spinner) v.findViewById(R.id.dropdown3);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.dropdown3, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        Button button = (Button) v.findViewById(R.id.button5);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Perhatian");
                final EditText text = (EditText) getActivity().findViewById(R.id.textfield4);
                final EditText text2 = (EditText) getActivity().findViewById(R.id.textfield5);
                final Spinner spinner3= (Spinner) getActivity().findViewById(R.id.dropdown3);
                pemilikrek = text.getText().toString();
                nominal = text2.getText().toString();

                //cek apakah nomer kosong
                if (pemilikrek.equals("")) {
                    alertDialog.setMessage("silahkan memasukkan nama pemilik rekening");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                } else

                if(nominal.equals("")) {
                    alertDialog.setMessage("silahkan memasukkan jumlah nominal top up");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                }else
                    //cek apakah nomer kurang dari 10 digit
                    if (Integer.parseInt(nominal) < 100000) {
                        alertDialog.setMessage("minimum top up adalah Rp. 100.000");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    } else {


                        //operator = prefixOperator(nomer);
                        bank = spinner3.getSelectedItem().toString();
                        alertDialog.setMessage("Apakah nama pemilik rek " + pemilikrek + " bank " + bank + " dan nominal top up " + nominal + " adalah benar?");
               /* alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Benar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });*/
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Benar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        SmsManager smsManager = SmsManager.getDefault();
                                        smsManager.sendTextMessage("082188881550", null, "topup//"+bank+"//"+nominal+"//"+pemilikrek, null, null);
                                        text.setText("");
                                        text2.setText("");
                                        Toast.makeText(getActivity(), "Permintaan Dikirim", Toast.LENGTH_LONG).show();
                                        spinner3.setSelection(0);

                                        Fragment newFragment = new LoadingFragment();
                                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                        ft.add(R.id.content_frame, newFragment).commit();

                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Salah",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                alertDialog.show();
            }
        });

        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Top Up Otomatis");
    }
}
