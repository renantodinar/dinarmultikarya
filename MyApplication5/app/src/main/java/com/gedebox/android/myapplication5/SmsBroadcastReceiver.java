package com.gedebox.android.myapplication5;

import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.SmsMessage;
import android.widget.TextView;
import android.widget.Toast;

public class SmsBroadcastReceiver extends BroadcastReceiver {
    /*protected MainActivity context2;

    public SmsBroadcastReceiver(){

    }

    public SmsBroadcastReceiver(MainActivity _context){
        context2 = _context;
    }*/

    MainActivity main = null;
    void setMainActivityHandler(MainActivity main){
        this.main=main;
    }

    public static final String SMS_BUNDLE = "pdus";

    String smsBody="";
    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            String smsMessageStr = "";
            for (int i = 0; i < sms.length; ++i) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

                smsBody = smsMessage.getMessageBody().toString();
                String address = smsMessage.getOriginatingAddress();

                //smsMessageStr += "MMBCSms: " + address + "\n";
                smsMessageStr += smsBody;
            }

            if(smsMessageStr.contains("ISI PULSA:")||smsMessageStr.contains("CEK SALDO:")||smsMessageStr.contains("TOP-UP SALDO:")){
                //temporary buble notification
                //Toast.makeText(context, smsMessageStr, Toast.LENGTH_LONG).show();
                //TextView tv=(TextView)main.findViewById(R.id.textview1);
                //tv.setText("test");
                //creating fragment object
                Fragment fragment = null;
                fragment = new SMSReceivedView();

                Bundle args = new Bundle();
                args.putString("smsbody", smsMessageStr);
                fragment.setArguments(args);

                if (fragment != null) {
                    FragmentTransaction ft = main.getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_frame, fragment);
                    ft.commit();
                }
            }

            //this will update the UI with message
            /*SmsActivity inst = SmsActivity.instance();
            inst.updateList(smsMessageStr);*/
        }
    }


        /*public boolean findString(String text,String text2) {
            //String strOrig = "Hello readers";
            boolean findit=false;
            int intIndex = text.indexOf(text2);
            if(intIndex == - 1){
                findit=false;
            }else{
                findit=true;
            }
            return findit;
        }*/

}
