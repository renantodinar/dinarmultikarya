package com.gedebox.android.myapplication5;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by andhika on 3/11/2016.
 */
public class Menu4 extends Fragment {
    String nomer=new String();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        //return inflater.inflate(R.layout.fragment_menu_4, container, false);
        View v = inflater.inflate(R.layout.fragment_menu_4, container, false);

        Button button = (Button) v.findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Perhatian");
                final EditText text = (EditText)getActivity().findViewById(R.id.textfield3);
                nomer= text.getText().toString();

                //cek apakah nomer kosong
                if(nomer.equals("")){
                    alertDialog.setMessage("silahkan memasukkan nomer pelanggan");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }else
                if(nomer.length()<12){
                    alertDialog.setMessage("jumlah angka no pelanggan tujuan salah/kurang dari 12 digit");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }else{
                    alertDialog.setMessage("Apakah no pelanggan " + nomer + " adalah benar? saldo tidak dapat dikembalikan apabila no pel yang dimasukkan salah");
               /* alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Benar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });*/
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Benar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    SmsManager smsManager = SmsManager.getDefault();
                                    smsManager.sendTextMessage("082188881550", null, "bayar//PLN//" + nomer, null, null);
                                    text.setText("");
                                    Toast.makeText(getActivity(), "Permintaan Dikirim", Toast.LENGTH_LONG).show();

                                    Fragment newFragment = new LoadingFragment();
                                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                    ft.add(R.id.content_frame, newFragment).commit();

                                    dialog.dismiss();
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Salah",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                }
                alertDialog.show();
            }
        });

        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Bayar PLN Bulanan");
    }
}
