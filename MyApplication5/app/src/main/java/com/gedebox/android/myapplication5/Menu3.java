package com.gedebox.android.myapplication5;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by andhika on 3/11/2016.
 */
public class Menu3 extends Fragment {
    String nomer=new String();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View v = inflater.inflate(R.layout.fragment_menu_3, container, false);

        Button button = (Button) v.findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText text = (EditText)getActivity().findViewById(R.id.textfield2);
                nomer= text.getText().toString();

                //cek apakah nomer kosong
                if(nomer.equals("")){
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Perhatian");
                    alertDialog.setMessage("silahkan memasukkan nomer pelanggan");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }else
                if(nomer.length()<12){
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Perhatian");
                    alertDialog.setMessage("jumlah angka no pelanggan tujuan salah/kurang dari 12 digit");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }else{
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage("082188881550", null, "tagihan//PLN//"+nomer, null, null);
                    text.setText("");
                    Toast.makeText(getActivity(), "Permintaan Dikirim", Toast.LENGTH_LONG).show();

                    Fragment newFragment = new LoadingFragment();
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.add(R.id.content_frame, newFragment).commit();
                }
            }
        });

        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Cek Tagihan PLN Bulanan");
    }
}
